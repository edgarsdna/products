CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(10) COLLATE utf8_latvian_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_latvian_ci NOT NULL,
  `price` int(8) NOT NULL,
  `created` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `created`) VALUES
(1, '1741561175', 'Garmin vivosmart 4 Black with Midnight Hardware Small/Medium 010-01995-00', 6999, 1587729153),
(2, '3628526553', 'Extendable Selfie Stick Monopod Tripod for Cell Phone + Bluetooth Remote Shutter', 898, 1587729218);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;